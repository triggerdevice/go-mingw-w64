ARG GO_VERSION=1.19.2
FROM golang:${GO_VERSION}-alpine
LABEL maintainer="mmurase@gmail.com"

RUN apk --no-cache add mingw-w64-binutils \
	mingw-w64-winpthreads \
	mingw-w64-headers \
	mingw-w64-crt \
	mingw-w64-gcc \
	make \
	git

ENV PATH=/go/bin:$PATH \
	CGO_ENABLED=1 \
	CXX_FOR_TARGET=x86_64-w64-mingw32-g++ \
	CC_FOR_TARGET=x86_64-w64-mingw32-gcc \
	CC=x86_64-w64-mingw32-gcc \
	GOOS=windows \
	GOARCH=amd64
WORKDIR /go
